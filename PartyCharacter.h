#ifndef PARTY_CHARACTER_H_INCLUDED
#define PARTY_CHARACTER_H_INCLUDED

#include "raylib.h"
#include "raymath.h"

#include "MoveComponent.h"

typedef struct PartyCharacter
{
    Model* model;
    ModelAnimation* anims;
    BoundingBox modelBoundingBox;
    unsigned int animsCount;
    int animFrameCounter;
    int startAnimFrame;
    int endAnimFrame;
    float animTimeCounter;
    float animFps;
    float secsPerFrame;
    
    Vector3 position;              /* player position */
    float scale;                   /* player scale */
    MoveComponent movecomp;
    
} PartyCharacter;

void InitPartyCharacter1();
void InitPartyCharacter2();

void UpdatePartyCharacter(PartyCharacter* pc, const float dt);
void DrawPartyCharacter(PartyCharacter* pc);

void DeinitPartyCharacter1();
void DeinitPartyCharacter2();

#endif /* PARTY_CHARACTER_H_INCLUDED */
