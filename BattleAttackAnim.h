#ifndef BATTLE_ATTACK_ANIM_H_INCLUDED
#define BATTLE_ATTACK_ANIM_H_INCLUDED

#include "raylib.h"

enum BattleAttackAnimState
{
    BTLATKANIMSTATE_RUN = 0, /* Run towards enemy */
    BTLATKANIMSTATE_ATK, /* Attack the enemy */
    BTLATKANIMSTATE_RUNBACK, /* Run back to their spot */
    BTLATKANIMSTATE_NUMSTATES
};

typedef struct BattleAttackAnim
{
    int player; /* player index */
    int enemy; /* enemy index */
    float animTime; /* current time into the anim */
    float runAnimTime; /* How long to run in front of enemy */
    float atkAnimTime; /* How long to attack the enemy */
    float runBackAnimTime; /* How long to run back */
    
    enum BattleAttackAnimState state;
    
    Sound hitSound;
} BattleAttackAnim;

void InitBattleAttackAnim();
void ResetBattleAttackAnim();
void UpdateBtlAtkAnim(const float dt);

#endif // BATTLE_ATTACK_ANIM_H_INCLUDED

