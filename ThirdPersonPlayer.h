#ifndef THIRD_PERSON_PLAYER_H_INCLUDED
#define THIRD_PERSON_PLAYER_H_INCLUDED

#include "raylib.h"
#include "raymath.h"

#include "AnimationComponent.h"
#include "MoveComponent.h"

typedef struct ThirdPersonPlayer
{
    Model* model;
    ModelAnimation* anims;
    BoundingBox modelBoundingBox;
    unsigned int animsCount;
    AnimationComponent animData;
    
    Texture2D* tex;
    Vector3 position;              /* player position */
    float scale;                   /* player scale */
    MoveComponent movecomp;
    
    float distance;             /* camera distance from player */
    float camyaw;               /* camera yaw */
    float campitch;             /* camera pitch */
    Camera3D camera;            /* raylib camera struct */
} ThirdPersonPlayer;

void InitThirdPersonPlayer(Model* model, ModelAnimation* anims, unsigned int animsCount);
void UpdateThirdPersonPlayer(const float dt);
void MoveThirdPersonPlayer(const float x, const float y, const float amount); /* Move with joystick controls */
void DrawThirdPersonPlayer();
void ThirdPersonPlayerAddPitch(const float amount);
void ThirdPersonPlayerAddYaw(const float amount);
void ThirdPersonPlayerSetAnimFrames(int startFrame, int endFrame); /* Resets anim if not already set to these vars */

#endif /* THIRD_PERSON_PLAYER_H_INCLUDED */
