#ifndef BATTLE_PLAYER_H_INCLUDED
#define BATTLE_PLAYER_H_INCLUDED

#include "AnimationComponent.h"

typedef struct BattlePlayer
{
    Model* model;
    ModelAnimation* anims;
    unsigned int animsCount;
    AnimationComponent animData;
    
    Vector3 position;
    float scale;            /* Model Scale */
    float yaw;              /* Model Y rotation in degrees */
    Vector3 battlePosition; /* where the position should reset to after an attack */
    char name[50];
    int attack; /* how much damage attack does */
    int health;
} BattlePlayer;
#define NUM_BATTLE_PLAYERS 3

void InitBattlePlayers();
void ResetBattlePlayers();
void DrawBattlePlayers();
void UpdateBattlePlayers(const float dt);

/* Resets a player's animation */
//void SetBattlePlayerAnimFrames(int player, int startFrame, int endFrame);

#endif /* BATTLE_PLAYER_H_INCLUDED */
