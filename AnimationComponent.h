#ifndef ANIMATION_COMPONENT_H_INCLUDED
#define ANIMATION_COMPONENT_H_INCLUDED

#include "raylib.h"

typedef struct AnimationComponent
{
    int animFrameCounter;
    int startAnimFrame;
    int endAnimFrame;
    float animTimeCounter;
    float animFps;
    float secsPerFrame;

    int idleStartFrame;
    int idleEndFrame;
    int runStartFrame;
    int runEndFrame;
    int attackStartFrame;
    int attackEndFrame;
} AnimationComponent;

static inline void UpdateAnimation(Model* m, ModelAnimation* ma, AnimationComponent* ad, const float dt)
{
    ad->animTimeCounter += dt;
    if (ad->animTimeCounter >= ad->secsPerFrame)
    {
        while (ad->animTimeCounter >= ad->secsPerFrame) { ad->animTimeCounter -= ad->secsPerFrame; }
        ad->animFrameCounter++;
        UpdateModelAnimation(*m, *ma, ad->animFrameCounter);
        if (ad->animFrameCounter >= ad->endAnimFrame) { ad->animFrameCounter = ad->startAnimFrame; }
    }
}

#endif /* ANIMATION_DATA_H_INCLUDED */
