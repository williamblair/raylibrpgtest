#include "Globals.h"
#include <stddef.h>

const int screenWidth = 800;
const int screenHeight = 450;

BattleMenu btlMenu = { 0 }; // default to 0 for all members
Camera3D camera = { 0 };
BattlePlayer battleplayers[NUM_BATTLE_PLAYERS];
Enemy enemies[NUM_ENEMIES];

BattleAttackAnim btlAtkAnim;

enum GameState gameState = GAMESTATE_DUNGEON;
enum BattleState battleState = BATTLESTATE_MENU;

ThirdPersonPlayer thrdPrsPlayer;
Model mainCharacterModel;
ModelAnimation* mainCharacterAnims = NULL;
unsigned int mainCharacterAnimsCount = 0;

Model  partyModel1;
ModelAnimation* partyModel1Anims = NULL;
unsigned int partyModel1AnimsCount = 0;
PartyCharacter partyCharacter1;

Model partyModel2;
ModelAnimation* partyModel2Anims = NULL;
unsigned int partyModel2AnimsCount = 0;
PartyCharacter partyCharacter2;

Model enemyModels[NUM_ENEMIES];
ModelAnimation* enemyAnims[NUM_ENEMIES] = { 0 };
unsigned int enemyAnimsCount[NUM_ENEMIES] = { 0 };

int numDungeonBoundingBoxes = 0;
BoundingBox dungeonBoundingBoxes[MAX_DUNGEON_BOUNDINGBOXES];
