#include <stddef.h>

#include "ThirdPersonPlayer.h"
#include "Globals.h"

void InitThirdPersonPlayer(Model* model, ModelAnimation* anims, unsigned int animsCount)
{
    ThirdPersonPlayer* tp = &thrdPrsPlayer;
    
    tp->tex = NULL;
    tp->model = model;
    tp->anims = anims;
    tp->animsCount = animsCount;
    tp->animData.animFrameCounter = 0;
    tp->animData.startAnimFrame = 0;
    tp->animData.endAnimFrame = 0;
    tp->animData.animTimeCounter = 0.0f;
    tp->animData.animFps = 30.0f;
    tp->animData.secsPerFrame = 1.0f / tp->animData.animFps;
    tp->scale = 0.025f;
    
    tp->position = Vec3(0.0f, 0.0f, 0.0f);
    tp->distance = 10.0f;
    tp->camyaw = 0.0f;
    tp->campitch = 0.0f;
    InitMoveComponent(&tp->movecomp);
    tp->movecomp.position = tp->position;
    tp->movecomp.target = Vector3Add(tp->position, tp->movecomp.forward);
    
    tp->camera.position = Vec3(10.0f, 0.0f, 0.0f);
    tp->camera.target = tp->position;
    tp->camera.up = Vec3(0.0f, 1.0f, 0.0f);
    tp->camera.fovy = 45.0f;
    tp->camera.projection = CAMERA_PERSPECTIVE;
    SetCameraMode(tp->camera, CAMERA_CUSTOM);
    tp->camyaw = 0.0f;
    tp->campitch = 0.0f;
    
    UpdateThirdPersonPlayer(0.0f);
}

void UpdateThirdPersonPlayer(const float dt)
{
    ThirdPersonPlayer* tp = &thrdPrsPlayer;
    Camera3D* cam = &tp->camera;
    
    Vector3 targetPos = tp->movecomp.position;
    // TODO - target offset
    //targetPos = Vector3Add(targetPos, )
    BoundingBox modelBox = GetModelBoundingBox(*tp->model);
    targetPos.y += modelBox.max.y * tp->scale + 1.0f;
    
    /* Calculate right axis */
    Matrix aboutY = MatrixRotateY(Deg2Rad(tp->camyaw));
    Vector3 xAxis = Vec3(1.0f, 0.0f, 0.0f);
    Vector3 right = Vector3Transform(xAxis, aboutY);
    right = Vector3Normalize(right);
    
    /* Calculate up axis */
    Matrix aboutRight = MatrixRotate(right, Deg2Rad(tp->campitch));
    Vector3 yAxis = Vec3(0.0f, 1.0f, 0.0f);
    Vector3 up = Vector3Transform(yAxis, aboutRight);
    up = Vector3Normalize(up);
    
    /* Calculate forward axis */
    Vector3 forward = Vector3CrossProduct(up, right);
    forward = Vector3Normalize(forward);
    
    /* Set location backwards from target */
    cam->position = Vector3Subtract(
        targetPos,
        Vector3Scale(forward, tp->distance)
    );
    
    /* Update view matrix/camera */
    cam->target = targetPos;
    cam->up = Vec3(0.0f, 1.0f, 0.0f); /* TODO - should this be set to up? */
    
    /* Update model animation */
    UpdateAnimation(tp->model, &tp->anims[0], &tp->animData, dt);
    
    /* Update player model bounding box */
    BoundingBox bb = GetModelBoundingBox(*tp->model);
    
    
    /* Swap Y/Z due to blender coordinates used
     * possibly TODO - just correct the axes in blender
     */
    float tmp = bb.min.y; bb.min.y = bb.min.z; bb.min.z = tmp;
    tmp = bb.max.y; bb.max.y = bb.max.z; bb.max.z = tmp;
    bb.min = Vector3Scale(bb.min, tp->scale);
    bb.max = Vector3Scale(bb.max, tp->scale);
    
    /* possibly todo? rotation */
    /* Don't include draw yaw offset/scaling */
    /*bb.min = Vector3RotateByQuaternion(bb.min, bbRot);
    bb.max = Vector3RotateByQuaternion(bb.max, bbRot);*/
    
    bb.min = Vector3Add(bb.min, tp->position);
    bb.max = Vector3Add(bb.max, tp->position);
    
    tp->modelBoundingBox = bb;
}

void MoveThirdPersonPlayer(const float x, const float y, const float amount)
{
    ThirdPersonPlayer* tp = &thrdPrsPlayer;
    
    const float xyLen = sqrtf(x*x + y*y);
    const float normX = x / xyLen;
    const float normY = y / xyLen;
    
    tp->movecomp.yaw = Rad2Deg(atan2f(normY, normX));
    tp->movecomp.yaw += tp->camyaw;
    /* By default we want to face forward down z axis (-z), not the x axis */
    tp->movecomp.yaw += 90.0f;
    while (tp->movecomp.yaw >= 360.0f) { tp->movecomp.yaw -= 360.0f; }
    while (tp->movecomp.yaw < 0.0f) { tp->movecomp.yaw += 360.0f; }
    
    tp->movecomp.pitch = 0.0f; /* No pitch allowed */
    UpdateMoveComponent(&tp->movecomp);
    MoveComponentForward(&tp->movecomp, amount);
    
    tp->position = tp->movecomp.position;
}

void DrawThirdPersonPlayer()
{
    Vector3 rotAxis;
    float rotRadians;
    ThirdPersonPlayer* tp = &thrdPrsPlayer;
    
    float drawYaw = -1.0f * tp->movecomp.yaw;
    while (drawYaw >= 360.0f) { drawYaw -= 360.0f; }
    while (drawYaw < 0.0f) { drawYaw += 360.0f; }
    Quaternion rot90X = QuaternionFromEuler(Deg2Rad(-90.0f), 0.0f, 0.0f); /* specific to this test model */
    Quaternion rotYaw = QuaternionFromEuler(0.0f, Deg2Rad(drawYaw), 0.0f); /* bj added yaw scale/offset */
    Quaternion resQuat = QuaternionMultiply(rotYaw, rot90X);
    QuaternionToAxisAngle(resQuat, &rotAxis, &rotRadians);
    
    DrawModelEx(
        *tp->model,
        tp->position,
        rotAxis,
        Rad2Deg(rotRadians),
        Vec3(tp->scale, tp->scale, tp->scale),
        WHITE
    );
    
    DrawBoundingBox(tp->modelBoundingBox, RED);
}

void ThirdPersonPlayerAddPitch(const float amount)
{
    thrdPrsPlayer.campitch += amount;
    thrdPrsPlayer.campitch = Clamp(thrdPrsPlayer.campitch, -80.0f, 80.0f);
}
void ThirdPersonPlayerAddYaw(const float amount)
{
    thrdPrsPlayer.camyaw += amount;
    while (thrdPrsPlayer.camyaw < 0.0f) { thrdPrsPlayer.camyaw += 360.0f; }
    while (thrdPrsPlayer.camyaw >= 360.0f) { thrdPrsPlayer.camyaw -= 360.0f; }
}

void ThirdPersonPlayerSetAnimFrames(int startFrame, int endFrame)
{
    ThirdPersonPlayer* tp = &thrdPrsPlayer;
    
    /* Prevent resetting already set */
    if (tp->animData.startAnimFrame != startFrame || tp->animData.endAnimFrame != endFrame)
    {
        tp->animData.startAnimFrame = startFrame;
        tp->animData.endAnimFrame = endFrame;
        tp->animData.animFrameCounter = tp->animData.startAnimFrame;
        tp->animData.animTimeCounter = 0.0f;
    }
}
