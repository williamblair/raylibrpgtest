#include "Globals.h"
#include <string.h>

void InitBattlePlayers()
{
    /* TODO - support models besides mc model */
#define InitPlayerStuff(player, modelPtr, animsPtr, animsCountIn) \
    player.model = modelPtr; \
    player.anims = animsPtr; \
    player.animsCount = animsCountIn; \
    player.animData.animFrameCounter = 0; \
    player.animData.startAnimFrame = MC_IDLE_ANIM_START_FRAME; \
    player.animData.endAnimFrame = MC_IDLE_ANIM_END_FRAME; \
    player.animData.animTimeCounter = 0.0f; \
    player.animData.animFps = 24.0f; \
    player.animData.secsPerFrame = 1.0f / player.animData.animFps; \
    player.animData.idleStartFrame = MC_IDLE_ANIM_START_FRAME; \
    player.animData.idleEndFrame = MC_IDLE_ANIM_END_FRAME; \
    player.animData.runStartFrame = MC_RUN_ANIM_START_FRAME; \
    player.animData.runEndFrame = MC_RUN_ANIM_END_FRAME; \
    player.animData.attackStartFrame = MC_ATTACK_ANIM_START_FRAME; \
    player.animData.attackEndFrame = MC_ATTACK_ANIM_END_FRAME; \
    player.scale = 0.025f; \
    player.yaw = 90.0f;
    
    InitPlayerStuff(battleplayers[0], &mainCharacterModel, mainCharacterAnims, mainCharacterAnimsCount)
    InitPlayerStuff(battleplayers[1], &partyModel1, partyModel1Anims, partyModel1AnimsCount)
    InitPlayerStuff(battleplayers[2], &partyModel2, partyModel2Anims, partyModel2AnimsCount)
#undef InitPlayerStuff
    
    battleplayers[0].position = Vec3(5.0f, 0.0f, 5.0f);
    battleplayers[1].position = Vec3(5.0f, 0.0f, 0.0f);
    battleplayers[2].position = Vec3(5.0f, 0.0f, -5.0f);
    battleplayers[0].battlePosition = Vec3(5.0f, 0.0f, 5.0f);
    battleplayers[1].battlePosition = Vec3(5.0f, 0.0f, 0.0f);
    battleplayers[2].battlePosition = Vec3(5.0f, 0.0f, -5.0f);
    
    strcpy(battleplayers[0].name, "PlayerA");
    strcpy(battleplayers[1].name, "PlayerB");
    strcpy(battleplayers[2].name, "PlayerC");
    
    battleplayers[0].attack = 50;
    battleplayers[1].attack = 50;
    battleplayers[2].attack = 50;
    battleplayers[0].health = 100;
    battleplayers[1].health = 100;
    battleplayers[2].health = 100;
}

void ResetBattlePlayers()
{
    /* TODO - support models besides mc model */
#define InitPlayerStuff(player, modelPtr, animsPtr, animsCountIn) \
    player.model = modelPtr; \
    player.anims = animsPtr; \
    player.animsCount = animsCountIn; \
    player.animData.animFrameCounter = 0; \
    player.animData.startAnimFrame = MC_IDLE_ANIM_START_FRAME; \
    player.animData.endAnimFrame = MC_IDLE_ANIM_END_FRAME; \
    player.animData.animTimeCounter = 0.0f; \
    player.animData.animFps = 24.0f; \
    player.animData.secsPerFrame = 1.0f / player.animData.animFps; \
    player.animData.idleStartFrame = MC_IDLE_ANIM_START_FRAME; \
    player.animData.idleEndFrame = MC_IDLE_ANIM_END_FRAME; \
    player.animData.runStartFrame = MC_RUN_ANIM_START_FRAME; \
    player.animData.runEndFrame = MC_RUN_ANIM_END_FRAME; \
    player.animData.attackStartFrame = MC_ATTACK_ANIM_START_FRAME; \
    player.animData.attackEndFrame = MC_ATTACK_ANIM_END_FRAME; \
    player.scale = 0.025f; \
    player.yaw = 90.0f;
    
    InitPlayerStuff(battleplayers[0], &mainCharacterModel, mainCharacterAnims, mainCharacterAnimsCount)
    InitPlayerStuff(battleplayers[1], &partyModel1, partyModel1Anims, partyModel1AnimsCount)
    InitPlayerStuff(battleplayers[2], &partyModel2, partyModel2Anims, partyModel2AnimsCount)
#undef InitPlayerStuff
    
    battleplayers[0].position = Vec3(5.0f, 0.0f, 5.0f);
    battleplayers[1].position = Vec3(5.0f, 0.0f, 0.0f);
    battleplayers[2].position = Vec3(5.0f, 0.0f, -5.0f);
    battleplayers[0].battlePosition = Vec3(5.0f, 0.0f, 5.0f);
    battleplayers[1].battlePosition = Vec3(5.0f, 0.0f, 0.0f);
    battleplayers[2].battlePosition = Vec3(5.0f, 0.0f, -5.0f);
}

static inline void DrawBattlePlayer(BattlePlayer* p)
{
    Vector3 rotAxis;
    float rotRadians;
    Quaternion rot90X = QuaternionFromEuler(Deg2Rad(-90.0f), 0.0f, 0.0f); /* specific to this test model */
    Quaternion rotYaw = QuaternionFromEuler(0.0f, Deg2Rad(p->yaw), 0.0f); /* bj added yaw scale/offset */
    Quaternion resQuat = QuaternionMultiply(rotYaw, rot90X);
    QuaternionToAxisAngle(resQuat, &rotAxis, &rotRadians);
    
    //DrawCube(p->position, 2.0f, 2.0f, 2.0f, RED);
    DrawModelEx(
        *p->model,
        p->position,
        rotAxis,
        Rad2Deg(rotRadians),
        Vec3(p->scale, p->scale, p->scale),
        WHITE
    );
}

void DrawBattlePlayers()
{
    int i;
    for (i = 0; i < NUM_BATTLE_PLAYERS; ++i)
    {
        DrawBattlePlayer(&battleplayers[i]);
    }
}

static inline void UpdateBattlePlayerAnim(BattlePlayer* p, const float dt)
{
    UpdateAnimation(p->model, &p->anims[0], &p->animData, dt);
}

void UpdateBattlePlayers(const float dt)
{
    int i;
    for (i = 0; i < NUM_BATTLE_PLAYERS; ++i)
    {
        UpdateBattlePlayerAnim(&battleplayers[i], dt);
    }
}

void SetBattlePlayerAnimFrames(int player, int startFrame, int endFrame)
{
    BattlePlayer* p = &battleplayers[player];
    
    /* Prevent resetting already set */
    if (p->animData.startAnimFrame != startFrame || p->animData.endAnimFrame != endFrame)
    {
        p->animData.startAnimFrame = startFrame;
        p->animData.endAnimFrame = endFrame;
        p->animData.animFrameCounter = p->animData.startAnimFrame;
        p->animData.animTimeCounter = 0.0f;
    }
}

