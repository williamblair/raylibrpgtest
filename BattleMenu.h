#ifndef BATTLE_MENU_H_INCLUDED
#define BATTLE_MENU_H_INCLUDED

#include "raylib.h"

enum BattleState
{
    BATTLESTATE_MENU = 0,
    BATTLESTATE_PLAYERANIM,
    BATTLESTATE_ENEMYTURN,
    BATTLESTATE_ENEMYANIM
};
enum BattleAction
{
    BATTLEACTION_ATTACK = 0,
    BATTLEACTION_DEFEND = 1,
    BATTLEACTION_NUMACTIONS = 2
};
enum BattleMenuState
{
    BATTLEMENUSTATE_TOPLEVEL = 0,
    BATTLEMENUSTATE_ENEMYSELECT = 1
};
typedef struct BattleMenu
{
    int state; /* BattleMenuState */
    
    int action; /* BattleAction */
    int player; /* player index into players of the current turn */
    int enemy; /* enemy index to attack (should be < NUM_ENEMIES) */
    
    Sound selectSound;
} BattleMenu;

void InitBattleMenu();
void ResetBattleMenu();
void DrawBattleMenu();
void BattleMenuInput();

#endif /* BATTLE_MENU_H_INCLUDED */
