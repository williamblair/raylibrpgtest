#include "MainCharacterModel.h"
#include "Globals.h"

#define MESHID_CARDBACK 1
#define MESHID_CARDFRONT 2
#define MESHID_HAIR 3
#define MESHID_MOUTH 4
#define MESHID_FOREHEAD 5
#define MESHID_RIGHTHANDFRONT 6
#define MESHID_RIGHTHANDBACK 7
#define MESHID_LEFTHANDBACK 8
#define MESHID_NECKFACE 9
#define MESHID_RIGHTLEG 10
#define MESHID_LEFTLEG 11
#define MESHID_UNKNOWN1 12
#define MESHID_UNKNOWN2 13
#define MESHID_INSIDECOATLMID1 14
#define MESHID_INSIDECOATLMID2 15
#define MESHID_INSIDECOATTOP1 16
#define MESHID_INSIDECOATTOP2 17
#define MESHID_INSIDECOATRMID1 18
#define MESHID_INSIDECOATRTOP1 19
#define MESHID_COATRIGHTWRIST 20
#define MESHID_COATBOTTOMWRISTS 21
#define MESHID_INNERSHIRT 22
#define MESHID_COATRIGHTARM 23
#define MESHID_COATLEFTARM 24
#define MESHID_OUTSIDECOATBR 25
#define MESHID_OUTSIDECOATMR 26
#define MESHID_OUTSIDECOATBL 27
#define MESHID_OUTSIDECOATML 28
#define MESHID_OUTSIDECOATMCL 29
#define MESHID_OUTSIDECOATTC 30
#define MESHID_OUTSIDECOATBC 31
#define MESHID_OUTSIDECOATMCR 32
#define MESHID_OUTSIDECOATCR 33
#define MESHID_OUTSIDECOATTF 34
#define MESHID_GLASSES 35
#define MESHID_GLASSESFRAME 36

static Texture2D mainCharacterModelBodyTex;
static Texture2D mainCharacterModelEyeTex;
static Texture2D mainCharacterModelFaceTex;
static Texture2D mainCharacterModelFootTex;
static Texture2D mainCharacterModelHairTex;
static Texture2D mainCharacterModelMouthTex;
static Texture2D mainCharacterModelMeganeTex;
static Texture2D mainCharacterModelCardBackTex;
static Texture2D mainCharacterModelCardFrontTex;

void InitMainCharacterModel()
{
    mainCharacterModel = LoadModel("./data/herovita/heroflipped.iqm");
    mainCharacterAnims = LoadModelAnimations("./data/herovita/heroflipped.iqm", &mainCharacterAnimsCount);
    
    mainCharacterModelBodyTex = LoadTexture("./data/herovita/diss_02.png");
    mainCharacterModelEyeTex = LoadTexture("./data/herovita/diss_05.png");
    mainCharacterModelFaceTex = LoadTexture("./data/herovita/diss_04.png");
    mainCharacterModelFootTex = LoadTexture("./data/herovita/diss_03.png");
    mainCharacterModelHairTex = LoadTexture("./data/herovita/diss_07.png");
    mainCharacterModelMouthTex = LoadTexture("./data/herovita/diss_06.png");
    mainCharacterModelMeganeTex = LoadTexture("./data/herovita/diss_00.png");
    mainCharacterModelCardBackTex = LoadTexture("./data/herovita/diss_08.png");
    mainCharacterModelCardFrontTex = LoadTexture("./data/herovita/diss_10.png");
    
    SetMaterialTexture(&mainCharacterModel.materials[0], MATERIAL_MAP_DIFFUSE, mainCharacterModelBodyTex);
    SetMaterialTexture(&mainCharacterModel.materials[1], MATERIAL_MAP_DIFFUSE, mainCharacterModelEyeTex);
    SetMaterialTexture(&mainCharacterModel.materials[2], MATERIAL_MAP_DIFFUSE, mainCharacterModelFaceTex);
    SetMaterialTexture(&mainCharacterModel.materials[3], MATERIAL_MAP_DIFFUSE, mainCharacterModelFootTex);
    SetMaterialTexture(&mainCharacterModel.materials[4], MATERIAL_MAP_DIFFUSE, mainCharacterModelHairTex);
    SetMaterialTexture(&mainCharacterModel.materials[5], MATERIAL_MAP_DIFFUSE, mainCharacterModelMouthTex);
    SetMaterialTexture(&mainCharacterModel.materials[6], MATERIAL_MAP_DIFFUSE, mainCharacterModelMeganeTex);
    SetMaterialTexture(&mainCharacterModel.materials[7], MATERIAL_MAP_DIFFUSE, mainCharacterModelCardBackTex);
    SetMaterialTexture(&mainCharacterModel.materials[8], MATERIAL_MAP_DIFFUSE, mainCharacterModelCardFrontTex);
    
    SetModelMeshMaterial(&mainCharacterModel, MESHID_CARDBACK, 7);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_CARDFRONT, 8);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_HAIR, 4);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_MOUTH, 5);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_FOREHEAD, 2);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_RIGHTHANDFRONT, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&mainCharacterModel, MESHID_RIGHTHANDBACK, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&mainCharacterModel, MESHID_LEFTHANDBACK, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&mainCharacterModel, MESHID_NECKFACE, 2);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_RIGHTLEG, 3);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_LEFTLEG, 3);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_INSIDECOATLMID1, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_INSIDECOATLMID2, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_INSIDECOATTOP1, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_INSIDECOATTOP2, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_INSIDECOATRMID1, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_INSIDECOATRTOP1, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_COATRIGHTWRIST, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_COATBOTTOMWRISTS, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_INNERSHIRT, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_COATRIGHTARM, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_COATLEFTARM, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATBR, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATMR, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATBL, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATML, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATMCL, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATTC, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATBC, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATMCR, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATCR, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_OUTSIDECOATTF, 0);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_GLASSES, 6);
    SetModelMeshMaterial(&mainCharacterModel, MESHID_GLASSESFRAME, 6);
}

void DeinitMainCharacterModel()
{
    UnloadTexture(mainCharacterModelBodyTex);
    UnloadTexture(mainCharacterModelEyeTex);
    UnloadTexture(mainCharacterModelFaceTex);
    UnloadTexture(mainCharacterModelFootTex);
    UnloadTexture(mainCharacterModelHairTex);
    UnloadTexture(mainCharacterModelMouthTex);
    UnloadTexture(mainCharacterModelMeganeTex);
    UnloadModelAnimations(mainCharacterAnims, mainCharacterAnimsCount);
    UnloadModel(mainCharacterModel);
}
