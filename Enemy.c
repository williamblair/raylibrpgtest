#include "Globals.h"
#include <stdio.h>
#include <string.h>

static Texture2D enemyModelTextures[12];

void InitEnemies()
{
    enemies[0].position = Vec3(-5.0f, 0.0f, 5.0f);
    enemies[1].position = Vec3(-5.0f, 0.0f, 0.0f);
    enemies[2].position = Vec3(-5.0f, 0.0f, -5.0f);
    enemies[0].battlePosition = Vec3(-5.0f, 0.0f, 5.0f);
    enemies[1].battlePosition = Vec3(-5.0f, 0.0f, 0.0f);
    enemies[2].battlePosition = Vec3(-5.0f, 0.0f, -5.0f);
    enemies[0].scale = 0.025f;
    enemies[1].scale = 0.025f;
    enemies[2].scale = 0.025f;
    enemies[0].yaw = -90.0f; /* Face down Z axis instead of X */ 
    enemies[1].yaw = -90.0f;
    enemies[2].yaw = -90.0f;
    
    strcpy(enemies[0].name, "EnemyA");
    strcpy(enemies[1].name, "EnemyB");
    strcpy(enemies[2].name, "EnemyC");
    
    enemies[0].health = 100;
    enemies[1].health = 100;
    enemies[2].health = 100;
    enemies[0].attack = 15;
    enemies[1].attack = 15;
    enemies[2].attack = 15;
    
    enemyModels[0] = LoadModel("./data/pyrojack/pyrojack.iqm");
    enemyAnims[0] = LoadModelAnimations("./data/pyrojack/pyrojack.iqm", &enemyAnimsCount[0]);
    enemyModels[1] = LoadModel("./data/pyrojack/pyrojack.iqm");
    enemyAnims[1] = LoadModelAnimations("./data/pyrojack/pyrojack.iqm", &enemyAnimsCount[1]);
    enemyModels[2] = LoadModel("./data/pyrojack/pyrojack.iqm");
    enemyAnims[2] = LoadModelAnimations("./data/pyrojack/pyrojack.iqm", &enemyAnimsCount[2]);
    
    enemies[0].model = &enemyModels[0];
    enemies[0].anims = enemyAnims[0];
    enemies[0].animsCount = enemyAnimsCount[0];
    enemies[1].model = &enemyModels[1];
    enemies[1].anims = enemyAnims[1];
    enemies[1].animsCount = enemyAnimsCount[1];
    enemies[2].model = &enemyModels[2];
    enemies[2].anims = enemyAnims[2];
    enemies[2].animsCount = enemyAnimsCount[2];
    
    enemyModelTextures[0] = LoadTexture("./data/pyrojack/diss_00.png");
    enemyModelTextures[1] = LoadTexture("./data/pyrojack/diss_01.png");
    enemyModelTextures[2] = LoadTexture("./data/pyrojack/diss_02.png");
    enemyModelTextures[3] = LoadTexture("./data/pyrojack/diss_03.png");
    enemyModelTextures[4] = LoadTexture("./data/pyrojack/diss_04.png");
    enemyModelTextures[5] = LoadTexture("./data/pyrojack/diss_05.png");
    enemyModelTextures[6] = LoadTexture("./data/pyrojack/diss_06.png");
    enemyModelTextures[7] = LoadTexture("./data/pyrojack/diss_07.png");
    enemyModelTextures[8] = LoadTexture("./data/pyrojack/diss_08.png");
    enemyModelTextures[9] = LoadTexture("./data/pyrojack/diss_09.png");
    enemyModelTextures[10] = LoadTexture("./data/pyrojack/diss_10.png");
    enemyModelTextures[11] = LoadTexture("./data/pyrojack/diss_11.png");
    //SetMaterialTexture(&enemies[0].model->materials[0], MATERIAL_MAP_DIFFUSE, enemyModelTextures[0]);
    //SetMaterialTexture(&enemies[1].model->materials[0], MATERIAL_MAP_DIFFUSE, enemyModelTextures[1]);
    //SetMaterialTexture(&enemies[2].model->materials[0], MATERIAL_MAP_DIFFUSE, enemyModelTextures[2]);
    
    printf("Enemy Model num meshes: %d\n", enemies[0].model->meshCount);
    
#define InitTextures(enIndex) \
    SetMaterialTexture(&enemies[enIndex].model->materials[0], MATERIAL_MAP_DIFFUSE, enemyModelTextures[0]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[1], MATERIAL_MAP_DIFFUSE, enemyModelTextures[1]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[2], MATERIAL_MAP_DIFFUSE, enemyModelTextures[2]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[3], MATERIAL_MAP_DIFFUSE, enemyModelTextures[3]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[4], MATERIAL_MAP_DIFFUSE, enemyModelTextures[4]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[5], MATERIAL_MAP_DIFFUSE, enemyModelTextures[5]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[6], MATERIAL_MAP_DIFFUSE, enemyModelTextures[6]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[7], MATERIAL_MAP_DIFFUSE, enemyModelTextures[7]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[8], MATERIAL_MAP_DIFFUSE, enemyModelTextures[8]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[9], MATERIAL_MAP_DIFFUSE, enemyModelTextures[9]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[10], MATERIAL_MAP_DIFFUSE, enemyModelTextures[10]); \
    SetMaterialTexture(&enemies[enIndex].model->materials[11], MATERIAL_MAP_DIFFUSE, enemyModelTextures[11]); \
    SetModelMeshMaterial(enemies[enIndex].model, 0, 11); \
    SetModelMeshMaterial(enemies[enIndex].model, 1, 10); \
    SetModelMeshMaterial(enemies[enIndex].model, 2, 9);  \
    SetModelMeshMaterial(enemies[enIndex].model, 3, 8);  \
    SetModelMeshMaterial(enemies[enIndex].model, 4, 7);  \
    SetModelMeshMaterial(enemies[enIndex].model, 5, 7);  \
    SetModelMeshMaterial(enemies[enIndex].model, 6, 6);  \
    SetModelMeshMaterial(enemies[enIndex].model, 7, 1);  \
    SetModelMeshMaterial(enemies[enIndex].model, 8, 5);  \
    SetModelMeshMaterial(enemies[enIndex].model, 9, 4);  \
    SetModelMeshMaterial(enemies[enIndex].model, 10, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 11, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 12, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 13, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 14, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 15, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 16, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 17, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 18, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 19, 3);  \
    SetModelMeshMaterial(enemies[enIndex].model, 20, 1);  \
    SetModelMeshMaterial(enemies[enIndex].model, 21, 2);  \
    SetModelMeshMaterial(enemies[enIndex].model, 22, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 23, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 24, 1);  \
    SetModelMeshMaterial(enemies[enIndex].model, 25, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 26, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 27, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 28, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 29, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 30, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 31, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 32, 0);  \
    SetModelMeshMaterial(enemies[enIndex].model, 33, 0);
    
    InitTextures(0)
    InitTextures(1)
    InitTextures(2)
    
#undef InitTextures

#define InitAnimData(data) \
    data.animFrameCounter = 204; \
    data.startAnimFrame = 204; \
    data.endAnimFrame = 298; \
    data.animTimeCounter = 0.0f; \
    data.animFps = 30.0f; \
    data.secsPerFrame = 1.0f / data.animFps; \
    data.idleStartFrame = 204; \
    data.idleEndFrame = 298; \
    data.runStartFrame = 204; \
    data.runEndFrame = 298; \
    data.attackStartFrame = 0; \
    data.attackEndFrame = 53;
    
    InitAnimData(enemies[0].animData)
    InitAnimData(enemies[1].animData)
    InitAnimData(enemies[2].animData)
    
#undef InitAnimData
}
void ResetEnemies()
{
    enemies[0].position = Vec3(-5.0f, 0.0f, 5.0f);
    enemies[1].position = Vec3(-5.0f, 0.0f, 0.0f);
    enemies[2].position = Vec3(-5.0f, 0.0f, -5.0f);
    enemies[0].battlePosition = Vec3(-5.0f, 0.0f, 5.0f);
    enemies[1].battlePosition = Vec3(-5.0f, 0.0f, 0.0f);
    enemies[2].battlePosition = Vec3(-5.0f, 0.0f, -5.0f);

    strcpy(enemies[0].name, "EnemyA");
    strcpy(enemies[1].name, "EnemyB");
    strcpy(enemies[2].name, "EnemyC");
    
    enemies[0].health = 100;
    enemies[1].health = 100;
    enemies[2].health = 100;
    enemies[0].attack = 15;
    enemies[1].attack = 15;
    enemies[2].attack = 15;
}

static inline void UpdateEnemy(Enemy* e, const float dt)
{
    UpdateAnimation(e->model, &e->anims[0], &e->animData, dt);
}

void UpdateEnemies(const float dt)
{
    int i;
    for (i = 0; i < NUM_ENEMIES; ++i)
    {
        if (enemies[i].health > 0) {
            UpdateEnemy(&enemies[i], dt);
        }
    }
}

static inline void DrawEnemy(Enemy* e)
{
    Vector3 rotAxis;
    float rotRadians;
    Quaternion rot90X = QuaternionFromEuler(Deg2Rad(-90.0f), 0.0f, 0.0f); /* specific to this test model */
    Quaternion rotYaw = QuaternionFromEuler(0.0f, Deg2Rad(e->yaw), 0.0f); /* bj added yaw scale/offset */
    Quaternion resQuat = QuaternionMultiply(rotYaw, rot90X);
    QuaternionToAxisAngle(resQuat, &rotAxis, &rotRadians);
    
    DrawModelEx(
        *e->model,
        e->position,
        rotAxis,
        Rad2Deg(rotRadians),
        Vec3(e->scale, e->scale, e->scale),
        WHITE
    );
}
void DrawEnemies()
{
    int i;
    for (i = 0; i < NUM_ENEMIES; ++i)
    {
        if (enemies[i].health > 0) {
            DrawEnemy(&enemies[i]);
        }
    }
}

void DeinitEnemies()
{
    UnloadTexture(enemyModelTextures[0]);
    UnloadTexture(enemyModelTextures[1]);
    UnloadTexture(enemyModelTextures[2]);
    UnloadModel(enemyModels[0]);
    UnloadModel(enemyModels[1]);
    UnloadModel(enemyModels[2]);
    UnloadModelAnimations(enemyAnims[0], enemyAnimsCount[0]);
    UnloadModelAnimations(enemyAnims[1], enemyAnimsCount[1]);
    UnloadModelAnimations(enemyAnims[2], enemyAnimsCount[2]);
}
