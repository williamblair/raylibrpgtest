#include "PartyCharacter.h"
#include "Globals.h"

/* Specific to main character model */
#define MESHID_CARDBACK 1
#define MESHID_CARDFRONT 2
#define MESHID_HAIR 3
#define MESHID_MOUTH 4
#define MESHID_FOREHEAD 5
#define MESHID_RIGHTHANDFRONT 6
#define MESHID_RIGHTHANDBACK 7
#define MESHID_LEFTHANDBACK 8
#define MESHID_NECKFACE 9
#define MESHID_RIGHTLEG 10
#define MESHID_LEFTLEG 11
#define MESHID_UNKNOWN1 12
#define MESHID_UNKNOWN2 13
#define MESHID_INSIDECOATLMID1 14
#define MESHID_INSIDECOATLMID2 15
#define MESHID_INSIDECOATTOP1 16
#define MESHID_INSIDECOATTOP2 17
#define MESHID_INSIDECOATRMID1 18
#define MESHID_INSIDECOATRTOP1 19
#define MESHID_COATRIGHTWRIST 20
#define MESHID_COATBOTTOMWRISTS 21
#define MESHID_INNERSHIRT 22
#define MESHID_COATRIGHTARM 23
#define MESHID_COATLEFTARM 24
#define MESHID_OUTSIDECOATBR 25
#define MESHID_OUTSIDECOATMR 26
#define MESHID_OUTSIDECOATBL 27
#define MESHID_OUTSIDECOATML 28
#define MESHID_OUTSIDECOATMCL 29
#define MESHID_OUTSIDECOATTC 30
#define MESHID_OUTSIDECOATBC 31
#define MESHID_OUTSIDECOATMCR 32
#define MESHID_OUTSIDECOATCR 33
#define MESHID_OUTSIDECOATTF 34
#define MESHID_GLASSES 35
#define MESHID_GLASSESFRAME 36

static Texture2D mainCharacterModelBodyTex;
static Texture2D mainCharacterModelEyeTex;
static Texture2D mainCharacterModelFaceTex;
static Texture2D mainCharacterModelFootTex;
static Texture2D mainCharacterModelHairTex;
static Texture2D mainCharacterModelMouthTex;
static Texture2D mainCharacterModelMeganeTex;
static Texture2D mainCharacterModelCardBackTex;
static Texture2D mainCharacterModelCardFrontTex;

static void InitPartyCharacter(PartyCharacter* pc, Model* model, ModelAnimation* anims, unsigned int animsCount)
{
    pc->model = model;
    pc->anims = anims;
    pc->animsCount = animsCount;
    pc->animFrameCounter = 0; // BJ TEST
    pc->startAnimFrame = 0;
    pc->endAnimFrame = anims[0].frameCount - 1;
    pc->animTimeCounter = 0.0f;
    pc->animFps = 30.0f;
    pc->secsPerFrame = 1.0f / pc->animFps;
    pc->scale = 0.025f;
    
    pc->position = Vec3(0.0f, 0.0f, 0.0f);
    InitMoveComponent(&pc->movecomp);
    pc->movecomp.position = pc->position;
    pc->movecomp.target = Vector3Add(pc->position, pc->movecomp.forward);
    
    UpdateThirdPersonPlayer(0.0f);
}

void InitPartyCharacter1()
{
    /* Same model as main character for now */
    partyModel1 = LoadModel("./data/herovita/heroflipped.iqm");
    partyModel1Anims = LoadModelAnimations("./data/herovita/heroflipped.iqm", &partyModel1AnimsCount);
    
    mainCharacterModelBodyTex = LoadTexture("./data/herovita/diss_02.png");
    mainCharacterModelEyeTex = LoadTexture("./data/herovita/diss_05.png");
    mainCharacterModelFaceTex = LoadTexture("./data/herovita/diss_04.png");
    mainCharacterModelFootTex = LoadTexture("./data/herovita/diss_03.png");
    mainCharacterModelHairTex = LoadTexture("./data/herovita/diss_07.png");
    mainCharacterModelMouthTex = LoadTexture("./data/herovita/diss_06.png");
    mainCharacterModelMeganeTex = LoadTexture("./data/herovita/diss_00.png");
    mainCharacterModelCardBackTex = LoadTexture("./data/herovita/diss_08.png");
    mainCharacterModelCardFrontTex = LoadTexture("./data/herovita/diss_10.png");
    
    SetMaterialTexture(&partyModel1.materials[0], MATERIAL_MAP_DIFFUSE, mainCharacterModelBodyTex);
    SetMaterialTexture(&partyModel1.materials[1], MATERIAL_MAP_DIFFUSE, mainCharacterModelEyeTex);
    SetMaterialTexture(&partyModel1.materials[2], MATERIAL_MAP_DIFFUSE, mainCharacterModelFaceTex);
    SetMaterialTexture(&partyModel1.materials[3], MATERIAL_MAP_DIFFUSE, mainCharacterModelFootTex);
    SetMaterialTexture(&partyModel1.materials[4], MATERIAL_MAP_DIFFUSE, mainCharacterModelHairTex);
    SetMaterialTexture(&partyModel1.materials[5], MATERIAL_MAP_DIFFUSE, mainCharacterModelMouthTex);
    SetMaterialTexture(&partyModel1.materials[6], MATERIAL_MAP_DIFFUSE, mainCharacterModelMeganeTex);
    SetMaterialTexture(&partyModel1.materials[7], MATERIAL_MAP_DIFFUSE, mainCharacterModelCardBackTex);
    SetMaterialTexture(&partyModel1.materials[8], MATERIAL_MAP_DIFFUSE, mainCharacterModelCardFrontTex);
    
    SetModelMeshMaterial(&partyModel1, MESHID_CARDBACK, 7);
    SetModelMeshMaterial(&partyModel1, MESHID_CARDFRONT, 8);
    SetModelMeshMaterial(&partyModel1, MESHID_HAIR, 4);
    SetModelMeshMaterial(&partyModel1, MESHID_MOUTH, 5);
    SetModelMeshMaterial(&partyModel1, MESHID_FOREHEAD, 2);
    SetModelMeshMaterial(&partyModel1, MESHID_RIGHTHANDFRONT, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&partyModel1, MESHID_RIGHTHANDBACK, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&partyModel1, MESHID_LEFTHANDBACK, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&partyModel1, MESHID_NECKFACE, 2);
    SetModelMeshMaterial(&partyModel1, MESHID_RIGHTLEG, 3);
    SetModelMeshMaterial(&partyModel1, MESHID_LEFTLEG, 3);
    SetModelMeshMaterial(&partyModel1, MESHID_INSIDECOATLMID1, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_INSIDECOATLMID2, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_INSIDECOATTOP1, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_INSIDECOATTOP2, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_INSIDECOATRMID1, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_INSIDECOATRTOP1, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_COATRIGHTWRIST, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_COATBOTTOMWRISTS, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_INNERSHIRT, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_COATRIGHTARM, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_COATLEFTARM, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATBR, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATMR, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATBL, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATML, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATMCL, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATTC, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATBC, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATMCR, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATCR, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_OUTSIDECOATTF, 0);
    SetModelMeshMaterial(&partyModel1, MESHID_GLASSES, 6);
    SetModelMeshMaterial(&partyModel1, MESHID_GLASSESFRAME, 6);
    
    InitPartyCharacter(&partyCharacter1, &partyModel1, partyModel1Anims, partyModel1AnimsCount);
}

void InitPartyCharacter2()
{
    partyModel2 = LoadModel("./data/herovita/heroflipped.iqm");
    partyModel2Anims = LoadModelAnimations("./data/herovita/heroflipped.iqm", &partyModel2AnimsCount);
    
    SetMaterialTexture(&partyModel2.materials[0], MATERIAL_MAP_DIFFUSE, mainCharacterModelBodyTex);
    SetMaterialTexture(&partyModel2.materials[1], MATERIAL_MAP_DIFFUSE, mainCharacterModelEyeTex);
    SetMaterialTexture(&partyModel2.materials[2], MATERIAL_MAP_DIFFUSE, mainCharacterModelFaceTex);
    SetMaterialTexture(&partyModel2.materials[3], MATERIAL_MAP_DIFFUSE, mainCharacterModelFootTex);
    SetMaterialTexture(&partyModel2.materials[4], MATERIAL_MAP_DIFFUSE, mainCharacterModelHairTex);
    SetMaterialTexture(&partyModel2.materials[5], MATERIAL_MAP_DIFFUSE, mainCharacterModelMouthTex);
    SetMaterialTexture(&partyModel2.materials[6], MATERIAL_MAP_DIFFUSE, mainCharacterModelMeganeTex);
    SetMaterialTexture(&partyModel2.materials[7], MATERIAL_MAP_DIFFUSE, mainCharacterModelCardBackTex);
    SetMaterialTexture(&partyModel2.materials[8], MATERIAL_MAP_DIFFUSE, mainCharacterModelCardFrontTex);
    
    SetModelMeshMaterial(&partyModel2, MESHID_CARDBACK, 7);
    SetModelMeshMaterial(&partyModel2, MESHID_CARDFRONT, 8);
    SetModelMeshMaterial(&partyModel2, MESHID_HAIR, 4);
    SetModelMeshMaterial(&partyModel2, MESHID_MOUTH, 5);
    SetModelMeshMaterial(&partyModel2, MESHID_FOREHEAD, 2);
    SetModelMeshMaterial(&partyModel2, MESHID_RIGHTHANDFRONT, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&partyModel2, MESHID_RIGHTHANDBACK, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&partyModel2, MESHID_LEFTHANDBACK, 2); /* Also contains hand tex */
    SetModelMeshMaterial(&partyModel2, MESHID_NECKFACE, 2);
    SetModelMeshMaterial(&partyModel2, MESHID_RIGHTLEG, 3);
    SetModelMeshMaterial(&partyModel2, MESHID_LEFTLEG, 3);
    SetModelMeshMaterial(&partyModel2, MESHID_INSIDECOATLMID1, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_INSIDECOATLMID2, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_INSIDECOATTOP1, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_INSIDECOATTOP2, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_INSIDECOATRMID1, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_INSIDECOATRTOP1, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_COATRIGHTWRIST, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_COATBOTTOMWRISTS, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_INNERSHIRT, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_COATRIGHTARM, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_COATLEFTARM, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATBR, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATMR, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATBL, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATML, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATMCL, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATTC, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATBC, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATMCR, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATCR, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_OUTSIDECOATTF, 0);
    SetModelMeshMaterial(&partyModel2, MESHID_GLASSES, 6);
    SetModelMeshMaterial(&partyModel2, MESHID_GLASSESFRAME, 6);
    
    
    InitPartyCharacter(&partyCharacter2, &partyModel2, partyModel2Anims, partyModel2AnimsCount);
}

void UpdatePartyCharacter(PartyCharacter* pc, const float dt)
{
     /* Update model animation */
    pc->animTimeCounter += dt;
    if (pc->animTimeCounter >= pc->secsPerFrame)
    {
        while (pc->animTimeCounter >= pc->secsPerFrame) { pc->animTimeCounter -= pc->secsPerFrame; }
        pc->animFrameCounter++;
        UpdateModelAnimation(*pc->model, pc->anims[0], pc->animFrameCounter);
        if (pc->animFrameCounter >= pc->endAnimFrame) { pc->animFrameCounter = pc->startAnimFrame; }
    }
    
    /* Update player model bounding box */
    BoundingBox bb = GetModelBoundingBox(*pc->model);
    
    /* Swap Y/Z due to blender coordinates used
     * possibly TODO - just correct the axes in blender
     */
    float tmp = bb.min.y; bb.min.y = bb.min.z; bb.min.z = tmp;
    tmp = bb.max.y; bb.max.y = bb.max.z; bb.max.z = tmp;
    bb.min = Vector3Scale(bb.min, pc->scale);
    bb.max = Vector3Scale(bb.max, pc->scale);
    
    /* possibly todo? rotation */
    /* Don't include draw yaw offset/scaling */
    /*bb.min = Vector3RotateByQuaternion(bb.min, bbRot);
    bb.max = Vector3RotateByQuaternion(bb.max, bbRot);*/
    
    bb.min = Vector3Add(bb.min, pc->position);
    bb.max = Vector3Add(bb.max, pc->position);
    
    pc->modelBoundingBox = bb;
}

void DrawPartyCharacter(PartyCharacter* pc)
{
    Vector3 rotAxis;
    float rotRadians;
    
    float drawYaw = -1.0f * pc->movecomp.yaw;
    while (drawYaw >= 360.0f) { drawYaw -= 360.0f; }
    while (drawYaw < 0.0f) { drawYaw += 360.0f; }
    Quaternion rot90X = QuaternionFromEuler(Deg2Rad(-90.0f), 0.0f, 0.0f); /* specific to this test model */
    Quaternion rotYaw = QuaternionFromEuler(0.0f, Deg2Rad(drawYaw), 0.0f); /* bj added yaw scale/offset */
    Quaternion resQuat = QuaternionMultiply(rotYaw, rot90X);
    QuaternionToAxisAngle(resQuat, &rotAxis, &rotRadians);
    
    DrawModelEx(
        *pc->model,
        pc->position,
        rotAxis,
        Rad2Deg(rotRadians),
        Vec3(pc->scale, pc->scale, pc->scale),
        WHITE
    );
    
    DrawBoundingBox(pc->modelBoundingBox, RED);
}

void DeinitPartyCharacter1()
{
    UnloadModel(partyModel1);
    UnloadModelAnimations(partyModel1Anims, partyModel1AnimsCount);
}

void DeinitPartyCharacter2()
{
    UnloadTexture(mainCharacterModelBodyTex);
    UnloadTexture(mainCharacterModelEyeTex);
    UnloadTexture(mainCharacterModelFaceTex);
    UnloadTexture(mainCharacterModelFootTex);
    UnloadTexture(mainCharacterModelHairTex);
    UnloadTexture(mainCharacterModelMouthTex);
    UnloadTexture(mainCharacterModelMeganeTex);
    UnloadModel(partyModel2);
    UnloadModelAnimations(partyModel2Anims, partyModel2AnimsCount);
}
