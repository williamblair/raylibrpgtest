#ifndef MOVE_COMPONENT_H_INCLUDED
#define MOVE_COMPONENT_H_INCLUDED

#include "raylib.h"
#include "raymath.h"

typedef struct MoveComponent
{
    Vector3 position;
    Vector3 target;
    Vector3 up;
    Vector3 forward;
    Vector3 right;
    float pitch; /* In degrees */
    float yaw; /* In degrees */
} MoveComponent;

void InitMoveComponent(MoveComponent* mc);
void UpdateMoveComponent(MoveComponent* mc);

static inline void MoveComponentForward(MoveComponent* mc, const float amount)
{
    mc->position = Vector3Add(mc->position, Vector3Scale(mc->forward, amount));
}
static inline void MoveComponentRight(MoveComponent* mc, const float amount)
{
    mc->position = Vector3Add(mc->position, Vector3Scale(mc->right, amount));
}
static inline void MoveComponentAddPitch(MoveComponent* mc, const float amount)
{
    mc->pitch += amount;
    mc->pitch = Clamp(mc->pitch, -80.0f, 80.0f);
}
static inline void MoveComponentAddYaw(MoveComponent* mc, const float amount)
{
    mc->yaw += amount;
    while (mc->yaw < 0.0f) { mc->yaw += 360.0f; }
    while (mc->yaw > 360.0f) { mc->yaw -= 360.0f; }
}

#endif /* MOVE_COMPONENT_H_INCLUDED */
