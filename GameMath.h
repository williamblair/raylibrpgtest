#ifndef GAME_MATH_H_INCLUDED
#define GAME_MATH_H_INCLUDED

#include "raylib.h"

#include <math.h>

// Helper declaration macro
#define Vec3(x,y,z) (Vector3){x,y,z}

#define Deg2Rad(val) ((val) * DEG2RAD)
#define Rad2Deg(val) ((val) * RAD2DEG)

#endif /* GAME_MATH_H_INCLUDED */
