#ifndef ENEMY_H_INCLUDED
#define ENEMY_H_INCLUDED

#include "AnimationComponent.h"

typedef struct Enemy
{
    Model* model;
    ModelAnimation* anims;
    unsigned int animsCount;
    AnimationComponent animData;
    
    Vector3 position;
    float scale;
    float yaw;
    Vector3 battlePosition; /* where the position should reset to after an attack */
    char name[50];
    int health;
    int attack;
} Enemy;
#define NUM_ENEMIES 3

void InitEnemies();
void ResetEnemies();
void UpdateEnemies(const float dt);
void DrawEnemies();
void DeinitEnemies();

#endif /* ENEMY_H_INCLUDED */
