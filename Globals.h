#ifndef GAME_GLOBALS_H_INCLUDED
#define GAME_GLOBALS_H_INCLUDED

#include "raylib.h"

#include "BattleMenu.h"
#include "BattlePlayer.h"
#include "Enemy.h"
#include "GameState.h"
#include "BattleAttackAnim.h"
#include "ThirdPersonPlayer.h"
#include "MainCharacterModel.h"
#include "PartyCharacter.h"

#include "GameMath.h"

extern const int screenWidth;// = 800;
extern const int screenHeight;// = 450;

extern BattleMenu btlMenu;// = { 0 }; // default to 0 for all members
extern Camera3D camera;// = { 0 };
extern BattlePlayer battleplayers[];//[NUM_PLAYERS];
extern Enemy enemies[];//[NUM_ENEMIES];

extern BattleAttackAnim btlAtkAnim;

extern enum GameState gameState;
extern enum BattleState battleState;//= BATTLESTATE_MENU

extern ThirdPersonPlayer thrdPrsPlayer;

extern Model mainCharacterModel;
extern ModelAnimation* mainCharacterAnims;
extern unsigned int mainCharacterAnimsCount;

extern Model partyModel1;
extern ModelAnimation* partyModel1Anims;
extern unsigned int partyModel1AnimsCount;
extern PartyCharacter partyCharacter1;

extern Model partyModel2;
extern ModelAnimation* partyModel2Anims;
extern unsigned int partyModel2AnimsCount;
extern PartyCharacter partyCharacter2;

extern Model enemyModels[];//NUM_ENEMIES
extern ModelAnimation* enemyAnims[];//NUM_ENEMIES
extern unsigned int enemyAnimsCount[];//NUM_ENEMIES

#define MAX_DUNGEON_BOUNDINGBOXES 20
extern int numDungeonBoundingBoxes;
extern BoundingBox dungeonBoundingBoxes[];

#endif /* GAME_GLOBALS_H_INCLUDED */
