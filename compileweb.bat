set SOURCES=main.c BattlePlayer.c Enemy.c Globals.c PlayerAttackAnim.c Battle.c MoveComponent.c ThirdPersonPlayer.c MainCharacterModel.c PartyCharacter.c
::set SFLAGS=-s USE_GLFW=3 -s ASYNCIFY
set SFLAGS=-s USE_GLFW=3
emcc -o game.html %SOURCES% -Os -Wall C:\raylibhtml\src\libraylib.a -I. -IC:\raylibhtml\src -L. -LC:\raylibhtml\src %SFLAGS% --preload-file data --shell-file ./minshell.html -DPLATFORM_WEB
