#include "raylib.h"
#include "raymath.h"
#include "Globals.h"

#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef PLATFORM_WEB
#include <emscripten/emscripten.h>
#endif

static bool appExit = false;

/* Dungeon variables */
int testTileMap[5][5] = {
    { 0, 0, 0, 0, 0 },
    { 0, 1, 1, 1, 0 },
    { 0, 1, 1, 1, 0 },
    { 0, 1, 1, 1, 0 },
    { 0, 0, 0, 0, 0 }
};
static const float tileSize = 5;
static const float tileBaseY = -tileSize / 2.0f;
static Music dungeonMusic;
static Texture2D floorTex;
static Texture2D wallTex;
static float nextBattleDist = 0.0f;
static float movedDist = 0.0f;
/****************************/

/* Battle variables */
Music battleMusic;
//static int enemyTurnEnemy; /* Which enemy's turn it is */
/****************************/

static void UpdateEnemyTurn()
{
    /* TODO - account for dead players */
    int enemyTurnPlayer = rand() % NUM_BATTLE_PLAYERS;
    
    //while (enemies[enemyTurnEnemy].health <= 0 && enemyTurnEnemy < NUM_ENEMIES) {
    //    ++enemyTurnEnemy;
    //}
    //if (enemyTurnEnemy >= NUM_ENEMIES) {
    //    printf("ERROR - enemyTurnEnemy >= num enemies\n");
    //}
    btlAtkAnim.player = enemyTurnPlayer; /* In this case the player is being attacked */
    //btlAtkAnim.enemy = enemyTurnEnemy;
    btlAtkAnim.animTime = 0.0f;
    btlAtkAnim.runAnimTime = 1.0f;
    btlAtkAnim.atkAnimTime = 1.0f;
    btlAtkAnim.runBackAnimTime = 1.0f;
    btlAtkAnim.state = BTLATKANIMSTATE_RUN;
    
    battleState = BATTLESTATE_ENEMYANIM;
    btlAtkAnim.state = BTLATKANIMSTATE_RUN;
    printf("End update enemy turn, battleState = enemyanim\n");
}

static void UpdateBattleLoop()
{
    const float dt = GetFrameTime();
    
    DisableCursor();
    
    appExit = IsKeyPressed(KEY_ESCAPE);
    
    UpdateMusicStream(battleMusic);
    
    if (IsKeyDown('Z')) {
        camera.target = (Vector3){ 0.0f, 0.0f, 0.f };
    }
    
    switch (battleState)
    {
    case BATTLESTATE_MENU:
        BattleMenuInput();
        break;
    case BATTLESTATE_PLAYERANIM:
        UpdateBtlAtkAnim(dt);
        break;
    case BATTLESTATE_ENEMYTURN:
        UpdateEnemyTurn();
        break;
    case BATTLESTATE_ENEMYANIM:
        /* Also handles enemy attack anim */
        UpdateBtlAtkAnim(dt);
        break;
    default:
        break;
    }
    
    UpdateCamera(&camera);
    UpdateEnemies(dt);
    UpdateBattlePlayers(dt);
    
    BeginDrawing();
        ClearBackground(RAYWHITE);
        
        BeginMode3D(camera);
            DrawBattlePlayers();
            DrawEnemies();
            DrawGrid(10, 1.0f);
        EndMode3D();
        
        DrawBattleMenu();
    EndDrawing();
}

static void BattleLoopInit()
{
    //PlayMusicStream(battleMusic);
    SetCameraMode(camera, CAMERA_ORBITAL);
    battleState = BATTLESTATE_MENU;
    
    ResetBattleMenu();
    ResetBattlePlayers();
    ResetEnemies();
    ResetBattleAttackAnim();
}

/* Returns true if the third person player model collides with 
 * a dungeon bounding box 
 */
static inline bool PlayerCollision()
{
    int i;
    for (i = 0; i < numDungeonBoundingBoxes; ++i)
    {
        if (CheckCollisionBoxes(dungeonBoundingBoxes[i], thrdPrsPlayer.modelBoundingBox)) {
            return true;
        }
    }
    return false;
}

static void UpdateDungeonLoop()
{
    int i, j;
    bool playerMoved = false;
    static const float moveSpeed = 8.0f;
    static const float rotSpeed = 30.0f;
    Vector2 mouseMove;
    
    const float dt = GetFrameTime();
    appExit = IsKeyPressed(KEY_ESCAPE);
    
    DisableCursor();
    
    UpdateMusicStream(dungeonMusic);
    
    /* If the player is attacking, check to see if the anim is finished */
    if (thrdPrsPlayer.animData.endAnimFrame == MC_ATTACK_ANIM_END_FRAME) {
        if (thrdPrsPlayer.animData.animFrameCounter >= thrdPrsPlayer.animData.endAnimFrame - 1) {
            ThirdPersonPlayerSetAnimFrames(MC_IDLE_ANIM_START_FRAME, MC_IDLE_ANIM_END_FRAME);
        }
    }
    
    /* Otherwise move the player */
    else {
        if (IsKeyDown(KEY_W)) {
            MoveThirdPersonPlayer(0.0f, -1.0f, moveSpeed * dt);
            movedDist += moveSpeed * dt;
            playerMoved = true;
            
            UpdateThirdPersonPlayer(0.0f);
            if (PlayerCollision()) {
                movedDist -= moveSpeed * dt;
                MoveThirdPersonPlayer(0.0f, 1.0f, moveSpeed * dt);
            }
        }
        else if (IsKeyDown(KEY_S)) {
            MoveThirdPersonPlayer(0.0f, 1.0f, moveSpeed * dt);
            movedDist += moveSpeed * dt;
            playerMoved = true;
            
            UpdateThirdPersonPlayer(0.0f);
            if (PlayerCollision()) {
                movedDist -= moveSpeed * dt;
                MoveThirdPersonPlayer(0.0f, -1.0f, moveSpeed * dt);
            }
        }
        if (IsKeyDown(KEY_A)) {
            MoveThirdPersonPlayer(-1.0f, 0.0f, moveSpeed * dt); 
            movedDist += moveSpeed * dt;
            playerMoved = true;
            
            UpdateThirdPersonPlayer(0.0f);
            if (PlayerCollision()) {
                movedDist -= moveSpeed * dt;
                MoveThirdPersonPlayer(1.0f, 0.0f, moveSpeed * dt);
            }
        }
        else if (IsKeyDown(KEY_D)) {
            MoveThirdPersonPlayer(1.0f, 0.0f, moveSpeed * dt);
            movedDist += moveSpeed * dt;
            playerMoved = true;
            
            UpdateThirdPersonPlayer(0.0f);
            if (PlayerCollision()) {
                movedDist -= moveSpeed * dt;
                MoveThirdPersonPlayer(-1.0f, 0.0f, moveSpeed * dt);
            }
        }
        
        if (playerMoved) {
            ThirdPersonPlayerSetAnimFrames(MC_RUN_ANIM_START_FRAME, MC_RUN_ANIM_END_FRAME);
        }
        else {
            ThirdPersonPlayerSetAnimFrames(MC_IDLE_ANIM_START_FRAME, MC_IDLE_ANIM_END_FRAME);
        }
        
        if (IsMouseButtonPressed(0)) {
            ThirdPersonPlayerSetAnimFrames(MC_ATTACK_ANIM_START_FRAME, MC_ATTACK_ANIM_END_FRAME);
        }
    }
    mouseMove = GetMouseDelta();
    if (fabsf(mouseMove.x) > 0.001f) {
        ThirdPersonPlayerAddYaw(mouseMove.x * rotSpeed * dt);
    }
    if (fabsf(mouseMove.y) > 0.001f) {
        ThirdPersonPlayerAddPitch(-mouseMove.y * rotSpeed * dt);
    }    
    
    UpdateThirdPersonPlayer(dt);
    UpdatePartyCharacter(&partyCharacter1, dt);
    UpdatePartyCharacter(&partyCharacter2, dt);
    
    /* Trigger battle if moved far enough */
    if (movedDist >= nextBattleDist) {
        gameState = GAMESTATE_BATTLE;
        return;
    }
    
    BeginDrawing();
        ClearBackground(RAYWHITE);

        BeginMode3D(thrdPrsPlayer.camera);
            /* Draw tilemap */
            for (i = 0; i < 5; ++i)
            {
                for (j = 0; j < 5; ++j)
                {
                    Texture2D* tex = testTileMap[i][j] ? &wallTex : &floorTex;
                    float posX = i * tileSize;
                    float posZ = j * tileSize;
                    float posY = testTileMap[i][j] ? (tileBaseY + tileSize) : tileBaseY;
                    
                    DrawCubeTexture(*tex, Vec3(posX, posY, posZ), tileSize, tileSize, tileSize, WHITE);
                }
            }
            
            /* DEBUG */
            #if 1
            for (i = 0; i < numDungeonBoundingBoxes; ++i)
            {
                DrawBoundingBox(dungeonBoundingBoxes[i], BLUE);
            }
            #endif
            
            DrawThirdPersonPlayer();
            
            DrawPartyCharacter(&partyCharacter1);
            DrawPartyCharacter(&partyCharacter2);
            
        EndMode3D();
        
    EndDrawing();
}

static void DungeonLoopInit()
{
    int i, j;
    
    /* How much movement player does until a battle occurs */
    nextBattleDist = (float)GetRandomValue(50, 100);
    movedDist = 0.0f; /* How much the player has moved */
    //PlayMusicStream(dungeonMusic);
    
    /* Initialize bounding boxes for player to hit */
    numDungeonBoundingBoxes = 0;
    for (i = 0; i < 5; ++i)
    {
        for (j = 0; j < 5; ++j)
        {
            if (testTileMap[i][j] == 1)
            {
                const float posX = i * tileSize;
                const float posZ = j * tileSize;
                const float posY = tileBaseY + tileSize;
                
                const float minX = posX - (tileSize / 2.0f);
                const float maxX = posX + (tileSize / 2.0f);
                const float minY = posY - (tileSize / 2.0f);
                const float maxY = posY + (tileSize / 2.0f);
                const float minZ = posZ - (tileSize / 2.0f);
                const float maxZ = posZ + (tileSize / 2.0f);
                
                dungeonBoundingBoxes[numDungeonBoundingBoxes].min = Vec3(minX, minY, minZ);
                dungeonBoundingBoxes[numDungeonBoundingBoxes].max = Vec3(maxX, maxY, maxZ);
                ++numDungeonBoundingBoxes;
            }
        }
    }
    
    ThirdPersonPlayerSetAnimFrames(MC_IDLE_ANIM_START_FRAME, MC_IDLE_ANIM_END_FRAME);
}

static enum GameState prevGameState = GAMESTATE_DUNGEON;
static bool gameStateChanged = false;
static void MainUpdateLoop()
{
    prevGameState = gameState;
    switch (gameState)
    {
    case GAMESTATE_BATTLE:
        if (gameStateChanged)
        {
            BattleLoopInit();
            gameStateChanged = false;
        }
        UpdateBattleLoop();
        break;
    case GAMESTATE_DUNGEON:
        if (gameStateChanged)
        {
            DungeonLoopInit();
            gameStateChanged = false;
        }
        UpdateDungeonLoop();
        //gameState = GAMESTATE_BATTLE;
        //gameStateChanged = true;
        break;
    default:
        break;
    }

    if (gameState != prevGameState)
    {
        if (prevGameState == GAMESTATE_DUNGEON)
        {
            StopMusicStream(dungeonMusic);
        }
        else if (prevGameState == GAMESTATE_BATTLE)
        {
            StopMusicStream(battleMusic);
        }
        
        gameStateChanged = true;
    }
}

int main()
{   
    InitWindow(screenWidth, screenHeight, "Test RPG");
    
    camera.position = Vec3(10.0f, 10.0f, 10.0f);
    camera.target = Vec3(0.f, 0.0f, 0.0f);
    camera.up = Vec3(0.0f, 1.0f, 0.0f);
    camera.fovy = 45.0f;
    camera.projection = CAMERA_PERSPECTIVE;
    SetCameraMode(camera, CAMERA_ORBITAL);
    
#ifndef PLATFORM_WEB
    SetTargetFPS(60);
#endif
    
    InitAudioDevice();
    
    InitBattlePlayers();
    InitEnemies();
    InitBattleMenu();
    InitBattleAttackAnim();
    
    dungeonMusic = LoadMusicStream("./data/dungeon.mp3");
    dungeonMusic.looping = true;
    floorTex = LoadTexture("./data/dungeonfloor.png");
    wallTex = LoadTexture("./data/dungeonwall.png");
    

    InitMainCharacterModel();
    InitThirdPersonPlayer(&mainCharacterModel, mainCharacterAnims, mainCharacterAnimsCount);
    ThirdPersonPlayerSetAnimFrames(MC_IDLE_ANIM_START_FRAME, MC_IDLE_ANIM_END_FRAME);
    printf("Model material count: %d\n", mainCharacterModel.materialCount);
    printf("Model mesh count: %d\n", mainCharacterModel.meshCount);
    
    InitPartyCharacter1();
    InitPartyCharacter2();
    partyCharacter1.movecomp.position = Vec3(-5.0f, 0.0f, 5.0f);
    partyCharacter1.movecomp.target = Vector3Add(partyCharacter1.movecomp.position, partyCharacter1.movecomp.forward);
    partyCharacter1.position = partyCharacter1.movecomp.position;
    partyCharacter2.movecomp.position = Vec3(-5.0f, 0.0f, 10.0f);
    partyCharacter2.movecomp.target = Vector3Add(partyCharacter2.movecomp.position, partyCharacter2.movecomp.forward);
    partyCharacter2.position = partyCharacter2.movecomp.position;
    
    battleMusic = LoadMusicStream("./data/battle.mp3");
    
    SetRandomSeed(time(0));
    
    gameState = GAMESTATE_DUNGEON;
    DungeonLoopInit();
    
#ifdef PLATFORM_WEB
    emscripten_set_main_loop(MainUpdateLoop, 0, 1);
#else
    while (!appExit)
    {
        MainUpdateLoop();
    }
#endif    
    
    UnloadMusicStream(dungeonMusic);
    UnloadMusicStream(battleMusic);
    UnloadTexture(floorTex);
    UnloadTexture(wallTex);
    DeinitMainCharacterModel();
    DeinitPartyCharacter1();
    DeinitPartyCharacter2();
    DeinitEnemies();
    CloseAudioDevice();
    CloseWindow();
    
    return 0;
}
