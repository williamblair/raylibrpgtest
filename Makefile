CC = gcc
#CFLAGS = -ggdb -s -static -std=c99 -Wall -DPLATFORM_DESKTOP
CFLAGS = -ggdb -std=c99 -Wall -DPLATFORM_DESKTOP
#INCDIRS = -IC:\raylib\raylib\src -Iexternal
INCDIRS = 
#LIBS = -lraylib -lopengl32 -lgdi32 -lwinmm
LIBS = -lraylib -ldl -pthread -lm
#          C:\raylib\raylib\src\raylib.rc.data
SOURCES = main.c \
          BattlePlayer.c \
          Enemy.c \
          Globals.c \
          BattleAttackAnim.c \
          BattleMenu.c \
          MoveComponent.c \
          ThirdPersonPlayer.c \
          MainCharacterModel.c \
          PartyCharacter.c
TARGET = main.exe
all:
	$(CC) -o $(TARGET) $(SOURCES) $(CFLAGS) $(INCDIRS) $(LIBS)
