#include "BattleMenu.h"
#include "Globals.h"
#include <stdio.h>
#include <string.h>

void InitBattleMenu()
{
    btlMenu.selectSound = LoadSound("./data/select.wav");
    btlMenu.state = BATTLEMENUSTATE_TOPLEVEL;
}

void ResetBattleMenu()
{
    btlMenu.state = BATTLEMENUSTATE_TOPLEVEL;
    btlMenu.player = 0;
}

void DrawBattleMenu()
{
    int fontSize = 16;
    
    if (battleState == BATTLESTATE_MENU)
    {
        /* Base menu */
        int menWidth = (int)((float)screenWidth * 0.25f);
        int menHeight = (int)((float)screenHeight * 0.2f);
        int tlX = 10;
        int tlY = screenHeight - menHeight - 10;
        DrawRectangle(tlX, tlY, menWidth, menHeight, BLUE);
        
        /* Text options */
        int textX = tlX + 10;
        int textY = tlY + 10;
        DrawText("Attack", textX, textY, fontSize, WHITE);
        DrawText("Defend", textX, textY + fontSize + 5, fontSize, WHITE);
        
        /* current top level selection */
        int selW = 20;
        int selH = 20;
        int selX = tlX + menWidth - selW - 10;
        int selY = textY + (fontSize + 5) * btlMenu.action;
        DrawRectangle(selX, selY, selW, selH, RED);

        /* Current player name */
        textX = tlX + 10;
        textY = tlY - fontSize - 10;
        DrawText(battleplayers[btlMenu.player].name, textX, textY, fontSize, RED);
        
        /* enemy selection submenu */
        if (btlMenu.state == BATTLEMENUSTATE_ENEMYSELECT)
        {
            int i;
            int selYs[NUM_ENEMIES];
            Vector2 enemyScreenPos;
            Enemy* en;

            /* Reset player selection if previously defeated enemy */
            if (enemies[btlMenu.enemy].health <= 0)
            {
                int i;
                for (i = 0; i < NUM_ENEMIES; ++i) {
                    if (enemies[i].health > 0) {
                        btlMenu.enemy = i;
                        break;
                    }
                }
            }
            en = &enemies[btlMenu.enemy];
            
            tlX = tlX + menWidth - 50;
            tlY = tlY + 50 - menHeight;
            DrawRectangle(tlX, tlY, menWidth, menHeight, GREEN);
            
            textX = tlX + 10;
            textY = tlY + 10;
            for (i = 0; i < NUM_ENEMIES; ++i)
            {
                if (enemies[i].health > 0) {
                    DrawText(enemies[i].name, textX, textY, fontSize, WHITE);
                    selYs[i] = textY; 
                    textY += fontSize + 5;
                }
            }
            
            selX = tlX + menWidth - selW - 10;
            selY = selYs[btlMenu.enemy];
            DrawRectangle(selX, selY, selW, selH, RED);
            
            // draw text above enemy
            enemyScreenPos = GetWorldToScreen(
                en->position,
                camera
            );
            textX = (int)enemyScreenPos.x - MeasureText("X", fontSize) / 2;
            textY = enemyScreenPos.y - 50;
            DrawText("X", textX, textY, fontSize, BLACK);
        }
    }

    /* Player health */
    int healthY = 10;
    int healthX = 10;
    int healthbarHeight = 20;
    int i;
    for (i = 0; i < NUM_BATTLE_PLAYERS; ++i)
    {
        char healthStr[128];
        sprintf(healthStr, "%s: %d/100",
            battleplayers[i].name,
            battleplayers[i].health);
        /* Draw Character Name and health numbers */
        DrawText(healthStr, healthX, healthY, fontSize / 2, BLACK);
        healthY += fontSize / 2 + 5;
        
        /* Draw healthbar */
        int totalHealthWidth = 200;
        int curHealthWidth = ((float)battleplayers[i].health / 100.0) * (float)totalHealthWidth;
        int missingHealthWidth = totalHealthWidth - curHealthWidth;
        if (curHealthWidth > 0) {
            DrawRectangle(healthX, healthY, curHealthWidth, healthbarHeight, GREEN);
        }
        if (missingHealthWidth > 0) {
            DrawRectangle(healthX + curHealthWidth, healthY, missingHealthWidth, healthbarHeight, RED);
        }

        healthY += healthbarHeight + 10;
    }
}

void BattleMenuInput()
{
    if (btlMenu.state == BATTLEMENUSTATE_TOPLEVEL)
    {
        
        if (IsKeyPressed(KEY_UP)) {
            btlMenu.action = btlMenu.action - 1;
            if (btlMenu.action < 0) {
                btlMenu.action = BATTLEACTION_NUMACTIONS - 1;
            }
            PlaySound(btlMenu.selectSound);
        }
        if (IsKeyPressed(KEY_DOWN)) {
            btlMenu.action = (btlMenu.action + 1) % BATTLEACTION_NUMACTIONS;
            PlaySound(btlMenu.selectSound);
        }
        
        if (IsKeyPressed(KEY_ENTER))
        {
            btlMenu.state = BATTLEMENUSTATE_ENEMYSELECT;
        }
    }
    
    else if (btlMenu.state == BATTLEMENUSTATE_ENEMYSELECT)
    {   
        if (IsKeyPressed(KEY_UP)) {
            do
            {
                btlMenu.enemy = btlMenu.enemy - 1;
                if (btlMenu.enemy < 0) {
                    btlMenu.enemy = NUM_ENEMIES - 1;
                }
            } while (enemies[btlMenu.enemy].health <= 0);
            PlaySound(btlMenu.selectSound);
        }
        if (IsKeyPressed(KEY_DOWN)) {
            do
            {
                btlMenu.enemy = (btlMenu.enemy + 1) % NUM_ENEMIES;
            } while (enemies[btlMenu.enemy].health <= 0);
            PlaySound(btlMenu.selectSound);
        }
        if (IsKeyPressed(KEY_ENTER)) {
            btlMenu.state = BATTLEMENUSTATE_TOPLEVEL;
            PlaySound(btlMenu.selectSound);
            
            /* Init player attack anim */
            btlAtkAnim.player = btlMenu.player;
            btlAtkAnim.enemy = btlMenu.enemy;
            //btlAtkAnim.totalAnimTime = 2.0f; // 2 seconds
            battleState = BATTLESTATE_PLAYERANIM;
        }
    }
}

