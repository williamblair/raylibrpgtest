#ifndef MAIN_CHARACTER_MODEL_H_INCLUDED
#define MAIN_CHARACTER_MODEL_H_INCLUDED

/* Specific to this test model */
#define MC_IDLE_ANIM_START_FRAME 0
#define MC_IDLE_ANIM_END_FRAME 45
#define MC_RUN_ANIM_START_FRAME 107
#define MC_RUN_ANIM_END_FRAME 130
#define MC_ATTACK_ANIM_START_FRAME 132
#define MC_ATTACK_ANIM_END_FRAME 174

void InitMainCharacterModel();
void DeinitMainCharacterModel();

#endif /* MAIN_CHARACTER_MODEL_H_INCLUDED */
