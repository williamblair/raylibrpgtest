#include "BattleAttackAnim.h"
#include "Globals.h"
#include "raymath.h"

#include <stdio.h>

// Calculate angle between two vectors in XY and XZ
// Copied from Windows install of raylib, Vector3Angle
static inline Vector2 Vector3AngleXYXZ(Vector3 v1, Vector3 v2)
{
    Vector2 result = { 0 }; 

    float dx = v2.x - v1.x;
    float dy = v2.y - v1.y;
    float dz = v2.z - v1.z;

    result.x = atan2f(dx, dz);                      // Angle in XZ
    result.y = atan2f(dy, sqrtf(dx*dx + dz*dz));    // Angle in XY

    return result;
}


void InitBattleAttackAnim()
{
    btlAtkAnim.hitSound = LoadSound("data/hit.wav");
    btlAtkAnim.player = 0;
    btlAtkAnim.enemy = 0;
    btlAtkAnim.animTime = 0.0f;
    btlAtkAnim.runAnimTime = 2.0f;
    btlAtkAnim.atkAnimTime = 2.0f;
    btlAtkAnim.runBackAnimTime = 2.0f;
    btlAtkAnim.state = BTLATKANIMSTATE_RUN;
}
void ResetBattleAttackAnim()
{
    btlAtkAnim.player = 0;
    btlAtkAnim.enemy = 0;
    btlAtkAnim.animTime = 0.0f;
    btlAtkAnim.state = BTLATKANIMSTATE_RUN;
}
void UpdateBtlAtkAnim(const float dt)
{
    int i;
    BattlePlayer* pl = &battleplayers[btlAtkAnim.player];
    Enemy* en = &enemies[btlAtkAnim.enemy];
    BattleAttackAnim* baa = &btlAtkAnim;
    
    baa->animTime += dt;
    
    switch (baa->state)
    {
    case BTLATKANIMSTATE_RUN:
    {
        float animTimeNorm = baa->animTime / baa->runAnimTime;
        /* Only be up to 75% close to the enemy */
        if (animTimeNorm > 0.75f) { animTimeNorm = 0.75f; }
        
        if (battleState == BATTLESTATE_PLAYERANIM)
        {
            if (battleplayers[btlMenu.player].animData.startAnimFrame !=
                battleplayers[btlMenu.player].animData.runStartFrame)
            {
                battleplayers[btlMenu.player].animData.startAnimFrame =
                    battleplayers[btlMenu.player].animData.runStartFrame;
                battleplayers[btlMenu.player].animData.endAnimFrame =
                    battleplayers[btlMenu.player].animData.runEndFrame;
                battleplayers[btlMenu.player].animData.animFrameCounter =
                    battleplayers[btlMenu.player].animData.runStartFrame;
                battleplayers[btlMenu.player].animData.animTimeCounter = 0.0f;
            }

            pl->position = Vector3Lerp(pl->battlePosition, en->position, animTimeNorm);
            
            Vector2 playerToEnemyAngles = Vector3AngleXYXZ(pl->position, en->position);
            pl->yaw = Rad2Deg(playerToEnemyAngles.x); /* XZ angle */
            pl->yaw -= 180.0f; /* Adjustment to look forward correctly */
            while (pl->yaw >= 360.0f) { pl->yaw -= 360.0f; }
            while (pl->yaw < 0.0f) { pl->yaw += 360.0f; }
            
            if (fabsf(baa->animTime - baa->runAnimTime) < 0.01f)
            {
                btlAtkAnim.state = BTLATKANIMSTATE_ATK;
                if (battleplayers[btlMenu.player].animData.startAnimFrame !=
                    battleplayers[btlMenu.player].animData.attackStartFrame)
                {
                    battleplayers[btlMenu.player].animData.startAnimFrame =
                        battleplayers[btlMenu.player].animData.attackStartFrame;
                    battleplayers[btlMenu.player].animData.endAnimFrame =
                        battleplayers[btlMenu.player].animData.attackEndFrame;
                    battleplayers[btlMenu.player].animData.animFrameCounter =
                        battleplayers[btlMenu.player].animData.attackStartFrame;
                    battleplayers[btlMenu.player].animData.animTimeCounter = 0.0f;
                }
                btlAtkAnim.animTime = 0.0f;
            }
        }
        else if (battleState == BATTLESTATE_ENEMYANIM)
        {
            if (enemies[btlAtkAnim.enemy].animData.startAnimFrame !=
                enemies[btlAtkAnim.enemy].animData.runStartFrame)
            {
                enemies[btlAtkAnim.enemy].animData.startAnimFrame =
                    enemies[btlAtkAnim.enemy].animData.runStartFrame;
                enemies[btlAtkAnim.enemy].animData.endAnimFrame =
                    enemies[btlAtkAnim.enemy].animData.runEndFrame;
                enemies[btlAtkAnim.enemy].animData.animFrameCounter =
                    enemies[btlAtkAnim.enemy].animData.runStartFrame;
                enemies[btlAtkAnim.enemy].animData.animTimeCounter = 0.0f;
            }
            
            en->position = Vector3Lerp(en->battlePosition, pl->position, animTimeNorm);
            
            /* TODO - angle */
            
            if (fabsf(baa->animTime - baa->runAnimTime) < 0.01f)
            {
                btlAtkAnim.state = BTLATKANIMSTATE_ATK;
                if (enemies[btlAtkAnim.enemy].animData.startAnimFrame !=
                    enemies[btlAtkAnim.enemy].animData.attackStartFrame)
                {
                    enemies[btlAtkAnim.enemy].animData.startAnimFrame =
                        enemies[btlAtkAnim.enemy].animData.attackStartFrame;
                    enemies[btlAtkAnim.enemy].animData.endAnimFrame =
                        enemies[btlAtkAnim.enemy].animData.attackEndFrame;
                    enemies[btlAtkAnim.enemy].animData.animFrameCounter =
                        enemies[btlAtkAnim.enemy].animData.attackStartFrame;
                    enemies[btlAtkAnim.enemy].animData.animTimeCounter = 0.0f;
                }
                btlAtkAnim.animTime = 0.0f;
            }
        }
        break;
    }
    case BTLATKANIMSTATE_ATK:
    {
        if (battleState == BATTLESTATE_PLAYERANIM)
        {
            //SetBattleBattleAnimFrames(btlMenu.player, MC_ATTACK_ANIM_START_FRAME, MC_ATTACK_ANIM_END_FRAME);
            if (battleplayers[btlMenu.player].animData.startAnimFrame !=
                battleplayers[btlMenu.player].animData.attackStartFrame)
            {
                battleplayers[btlMenu.player].animData.startAnimFrame =
                    battleplayers[btlMenu.player].animData.attackStartFrame;
                battleplayers[btlMenu.player].animData.endAnimFrame =
                    battleplayers[btlMenu.player].animData.attackEndFrame;
                battleplayers[btlMenu.player].animData.animFrameCounter =
                    battleplayers[btlMenu.player].animData.attackStartFrame;
                battleplayers[btlMenu.player].animData.animTimeCounter = 0.0f;
            }
            if (fabsf(baa->animTime - baa->atkAnimTime) < 0.01f)
            {
                //PlaySound(btlAtkAnim.hitSound);
                en->health -= pl->attack;
                baa->state = BTLATKANIMSTATE_RUNBACK;
                //SetBattleBattleAnimFrames(btlMenu.player, MC_RUN_ANIM_START_FRAME, MC_RUN_ANIM_END_FRAME);
                if (battleplayers[btlMenu.player].animData.startAnimFrame !=
                    battleplayers[btlMenu.player].animData.runStartFrame)
                {
                    battleplayers[btlMenu.player].animData.startAnimFrame =
                        battleplayers[btlMenu.player].animData.runStartFrame;
                    battleplayers[btlMenu.player].animData.endAnimFrame =
                        battleplayers[btlMenu.player].animData.runEndFrame;
                    battleplayers[btlMenu.player].animData.animFrameCounter =
                        battleplayers[btlMenu.player].animData.runStartFrame;
                    battleplayers[btlMenu.player].animData.animTimeCounter = 0.0f;
                }
                baa->animTime = 0.0f;
            }
        }
        else if (battleState == BATTLESTATE_ENEMYANIM)
        {
            if (fabsf(baa->animTime - baa->atkAnimTime) < 0.01f)
            {
                pl->health -= en->attack;
                if (pl->health < 0) { pl->health = 0; }

                baa->state = BTLATKANIMSTATE_RUNBACK;
                if (enemies[btlAtkAnim.enemy].animData.startAnimFrame !=
                    enemies[btlAtkAnim.enemy].animData.runStartFrame)
                {
                    enemies[btlAtkAnim.enemy].animData.startAnimFrame =
                        enemies[btlAtkAnim.enemy].animData.runStartFrame;
                    enemies[btlAtkAnim.enemy].animData.endAnimFrame =
                        enemies[btlAtkAnim.enemy].animData.runEndFrame;
                    enemies[btlAtkAnim.enemy].animData.animFrameCounter =
                        enemies[btlAtkAnim.enemy].animData.runStartFrame;
                    enemies[btlAtkAnim.enemy].animData.animTimeCounter = 0.0f;
                }
                baa->animTime = 0.0f;
            }
        }
        break;
    }
    case BTLATKANIMSTATE_RUNBACK:
    {
        float animTimeNorm = baa->animTime / baa->runBackAnimTime;
            
        /* Don't be any nearer to the enemy than 25% */
        if (animTimeNorm < 0.25f) { animTimeNorm = 0.25f; }
        
        if (battleState == BATTLESTATE_PLAYERANIM)
        {
            //SetBattleBattleAnimFrames(btlMenu.player, MC_RUN_ANIM_START_FRAME, MC_RUN_ANIM_END_FRAME);
            if (battleplayers[btlMenu.player].animData.startAnimFrame !=
                battleplayers[btlMenu.player].animData.runStartFrame)
            {
                battleplayers[btlMenu.player].animData.startAnimFrame =
                    battleplayers[btlMenu.player].animData.runStartFrame;
                battleplayers[btlMenu.player].animData.endAnimFrame =
                    battleplayers[btlMenu.player].animData.runEndFrame;
                battleplayers[btlMenu.player].animData.animFrameCounter =
                    battleplayers[btlMenu.player].animData.runStartFrame;
                battleplayers[btlMenu.player].animData.animTimeCounter = 0.0f;
            }
            
            pl->position = Vector3Lerp(en->position, pl->battlePosition, animTimeNorm);
            
            Vector2 playerToEnemyAngles = Vector3AngleXYXZ(en->position, pl->position);
            pl->yaw = Rad2Deg(playerToEnemyAngles.x); /* XZ angle */
            pl->yaw -= 180.0f; /* adjustment to look forward correctly */
            while (pl->yaw >= 360.0f) { pl->yaw -= 360.0f; }
            while (pl->yaw < 0.0f) { pl->yaw += 360.0f; }
            
            if (fabsf(baa->animTime - baa->runBackAnimTime) < 0.01f)
            {
                ResetBattleAttackAnim();
                pl->position = pl->battlePosition;
                pl->yaw = 90.0f; /* Default yaw */
                
                /* Check if all enemies defeated */
                for (i = 0; i < NUM_ENEMIES; ++i)
                {
                    if (enemies[i].health > 0) {
                        break;
                    }
                }
                if (i < NUM_ENEMIES) {
                    
                    //SetBattleBattleAnimFrames(btlMenu.player,  MC_IDLE_ANIM_START_FRAME, MC_IDLE_ANIM_END_FRAME);
                    if (battleplayers[btlMenu.player].animData.startAnimFrame !=
                        battleplayers[btlMenu.player].animData.idleStartFrame)
                    {
                        battleplayers[btlMenu.player].animData.startAnimFrame =
                            battleplayers[btlMenu.player].animData.idleStartFrame;
                        battleplayers[btlMenu.player].animData.endAnimFrame =
                            battleplayers[btlMenu.player].animData.idleEndFrame;
                        battleplayers[btlMenu.player].animData.animFrameCounter =
                            battleplayers[btlMenu.player].animData.idleStartFrame;
                        battleplayers[btlMenu.player].animData.animTimeCounter = 0.0f;
                    }
                    
                    //btlMenu.player = (btlMenu.player + 1) % NUM_BATTLE_PLAYERS;
                    do
                    {
                        ++btlMenu.player;
                    } while (btlMenu.player < NUM_BATTLE_PLAYERS &&
                            battleplayers[btlMenu.player].health <= 0);
                    if (btlMenu.player < NUM_BATTLE_PLAYERS) {
                        battleState = BATTLESTATE_MENU;
                    }
                    else {
                        battleState = BATTLESTATE_ENEMYTURN;
                        
                        int firstTurnEnemy = 0;
                        while (enemies[firstTurnEnemy].health <= 0) { ++firstTurnEnemy; }
                        btlAtkAnim.enemy = firstTurnEnemy;
                    }
                }
                else {
                    /* TODO - end of battle screen */
                    gameState = GAMESTATE_DUNGEON;
                    battleState = BATTLESTATE_MENU;
                }
            }
        }
        
        else if (battleState == BATTLESTATE_ENEMYANIM)
        {
            
            en->position = Vector3Lerp(pl->position, en->battlePosition, animTimeNorm);
            
            /* TODO - angle to player */
            
            if (fabsf(baa->animTime - baa->runBackAnimTime) < 0.01f)
            {
                en->position = en->battlePosition;
                if (enemies[btlAtkAnim.enemy].animData.startAnimFrame !=
                    enemies[btlAtkAnim.enemy].animData.idleStartFrame)
                {
                    enemies[btlAtkAnim.enemy].animData.startAnimFrame =
                        enemies[btlAtkAnim.enemy].animData.idleStartFrame;
                    enemies[btlAtkAnim.enemy].animData.endAnimFrame =
                        enemies[btlAtkAnim.enemy].animData.idleEndFrame;
                    enemies[btlAtkAnim.enemy].animData.animFrameCounter =
                        enemies[btlAtkAnim.enemy].animData.idleStartFrame;
                    enemies[btlAtkAnim.enemy].animData.animTimeCounter = 0.0f;
                }
                /* TODO - default yaw */
                
                do
                {
                    ++btlAtkAnim.enemy;
                } while(btlAtkAnim.enemy < NUM_ENEMIES &&
                        enemies[btlAtkAnim.enemy].health <= 0);
                
                /* Enemy turns done */
                if (btlAtkAnim.enemy >= NUM_ENEMIES) {
                    ResetBattleAttackAnim();
                    battleState = BATTLESTATE_MENU;
                    btlMenu.player = 0;
                }
                else {
                    battleState = BATTLESTATE_ENEMYTURN;
                }
                
                btlAtkAnim.animTime = 0.0f;
            }
        }
        break;
    }
    default:
        break;
    }
}
