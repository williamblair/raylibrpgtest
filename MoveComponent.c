#include "MoveComponent.h"
#include "GameMath.h"

void InitMoveComponent(MoveComponent* mc)
{
    mc->position = Vec3(0.0f, 0.0f, 0.0f);
    mc->target = Vec3(0.0f, 0.0f, -1.0f); /* face down Z */
    mc->up = Vec3(0.0f, 1.0f, 0.0f);
    mc->forward = Vec3(0.0f, 0.0f, -1.0f);
    mc->right = Vec3(1.0f, 0.0f, 0.0f);
    mc->pitch = 0.0f; /* In degrees */
    mc->yaw = 0.0f; /* In degrees */
}

void UpdateMoveComponent(MoveComponent* mc)
{
    /* Calculate right axis */
    Matrix aboutY = MatrixRotateY(Deg2Rad(mc->yaw));
    Vector3 xAxis = Vec3(1.0f, 0.0f, 0.0f);
    mc->right = Vector3Transform(xAxis, aboutY);
    mc->right = Vector3Normalize(mc->right);
    
    /* Calculate up axis */
    Matrix aboutRight = MatrixRotate(mc->right, Deg2Rad(mc->pitch));
    Vector3 yAxis = Vec3(0.0f, 1.0f, 0.0f);
    mc->up = Vector3Transform(yAxis, aboutRight);
    mc->up = Vector3Normalize(mc->up);
    
    /* Calculate forward axis */
    mc->forward = Vector3CrossProduct(mc->up, mc->right);
    mc->forward = Vector3Normalize(mc->forward);
    
    /* Calculate target position */
    mc->target = Vector3Add(mc->position, mc->forward);
    
#if 0
    RMAPI Vector3 Vector3CrossProduct(Vector3 v1, Vector3 v2)
    RMAPI Vector3 Vector3Normalize(Vector3 v)
    // Transforms a Vector3 by a given Matrix
    RMAPI Vector3 Vector3Transform(Vector3 v, Matrix mat)
    // Transform a vector by quaternion rotation
    RMAPI Vector3 Vector3RotateByQuaternion(Vector3 v, Quaternion q)
    // Lerp two Vec3s
    RMAPI Vector3 Vector3Lerp(Vector3 v1, Vector3 v2, float amount)
#endif
}
